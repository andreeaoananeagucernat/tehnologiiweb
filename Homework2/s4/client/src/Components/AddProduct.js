import React, { Component } from 'react';
import axios from 'axios';

class AddProduct extends Component 
{
    constructor(props){
          super(props);
          this.state={
              product:"",
              price:""
          }
          this.handleSubmit=this.handleSubmit.bind(this);
          this.handleChange=this.handleChange.bind(this);
          this.handleChange2=this.handleChange2.bind(this);
          
      }
      
      
      handleSubmit(e){
          e.preventDefault();
          let obj=
		  {
		  productName:this.state.product,
          price:this.state.price}
          
          axios.post('https://webtech2018-andreeaoananeagucernat.c9users.io:8081/add',obj) 
          .then((res)=>
          {
              if(res.status===200){
                  
                  this.props.productAdded(res.data);
              }
          }).catch((err)=>
          {
              console.log(err)
          })
         
      }
      handleChange(e){
          this.setState({product:e.target.value})
      }
      handleChange2(e){
          this.setState({price:e.target.value})
      }
  render() {
      
    return (
      <div className="addProduct">
       <form className="formAdd" onSubmit={this.handleSubmit}>
       <input type="text" placeholder="product" onChange={this.handleChange}/>
       <input type="text" placeholder="price" onChange={this.handleChange2}/>
       <button type="submit">Add product</button>
       </form>
      </div>
    );
  }
}
export default AddProduct;
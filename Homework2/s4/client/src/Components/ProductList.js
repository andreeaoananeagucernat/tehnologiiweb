import React, { Component } from 'react';


class ProductList extends Component 
{
    constructor(props){
          super(props);
      }
  render() 
  {
       let products = this.props.products.map((product, index) =>{
            return <div key={index}>{product.productName}->{product.price}</div>
       });
    return (
      <div className="productList">
       {products}
      </div>
    );
  }
}

export default ProductList;

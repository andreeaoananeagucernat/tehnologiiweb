import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddProduct from './Components/AddProduct.js';
import ProductList from './Components/ProductList.js';
import axios from 'axios';

class App extends Component {
  constructor(props){
    super(props);
    this.state={};
    this.state.productList=[];
  }
  onProductAdded=(product)=>{
    let productList=this.state.productList;
    productList.push(product);
    this.setState({productList:productList});
  }
  
   componentWillMount(){ 
        axios.get("https://webtech2018-andreeaoananeagucernat.c9users.io:8081/get-all");
        .then((res)=> {this.setState({productList:res.data})})
        .catch((err)=>
        {
            console.log(err);
        })
    } 
  render() {
    return (
     
      <div className="App">
       <AddProduct productAdded={this.onProductAdded}/>
       <ProductList productList={this.state.productList}/>
      </div>
    );
  }
}
export default App;
